// .prettierrc.js
module.exports = {
    singleQuote: true,
    printWidth: 110,
    arrowParens: 'avoid',
    trailingComma: 'none',
    tabWidth: 4
};
