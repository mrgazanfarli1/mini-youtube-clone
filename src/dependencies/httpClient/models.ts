/* eslint-disable */
import { AxiosRequestConfig, AxiosResponse } from 'axios';
import { AnySchema } from 'yup';

export type RequestMethod = 'get' | 'post' | 'put' | 'delete' | 'patch';

export interface IRequestConfig extends AxiosRequestConfig {
    refreshTokenOnExpiry?: boolean;
    uuid?: string;
}

export interface IConfigPresets {
    default: IRequestConfig;
    increasedTimeout: IRequestConfig;
    multipartFormData: IRequestConfig;
    blobResponse: IRequestConfig;
}

export interface IHttpClient {
    requestAPI: <Request, Response>(
        method: RequestMethod,
        url: string,
        data: Request,
        config?: IRequestConfig,
        validationSchema?: AnySchema,
        authHeadersMode?: 'none' | 'refresh' | 'access'
    ) => Promise<AxiosResponse<Response>>;

    get: <Response>(url: string, config?: IRequestConfig, validationSchema?: AnySchema) => Promise<Response>;

    post: <Request, Response>(
        url: string,
        data?: Request,
        config?: IRequestConfig,
        validationSchema?: AnySchema
    ) => Promise<Response>;

    put: <Request, Response>(
        url: string,
        data?: Request,
        config?: IRequestConfig,
        validationSchema?: AnySchema
    ) => Promise<Response>;

    delete: <Response>(
        url: string,
        config?: IRequestConfig,
        validationSchema?: AnySchema
    ) => Promise<Response>;

    patch: <Request, Response>(
        url: string,
        data?: Request,
        config?: IRequestConfig,
        validationSchema?: AnySchema
    ) => Promise<Response>;

    readonly configPresets: IConfigPresets;
}
