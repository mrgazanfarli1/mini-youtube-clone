export enum ERequestStatus {
    ERROR = 'ERROR',
    IDLE = 'IDLE',
    PENDING = 'PENDING',
    SUCCESS = 'SUCCESS'
}
