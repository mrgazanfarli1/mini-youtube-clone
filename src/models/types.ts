export type Maybe<T> = T | null | undefined;

export type TThumbnailKey = 'default' | 'medium' | 'high' | 'standard' | 'maxres';

export type TLiveBroadCastContent = 'upcoming' | 'live' | 'none';
