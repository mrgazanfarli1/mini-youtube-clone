import { ERequestStatus } from 'models/enums';
import { Maybe, TLiveBroadCastContent, TThumbnailKey } from 'models/types';

export interface IAsyncDataBase {
    error: Maybe<IError>;
    status: ERequestStatus;
}

export interface IAsyncData<T> extends IAsyncDataBase {
    data: Maybe<T>;
}

export interface IError {
    code?: string;
    httpCode?: number;
    error?: boolean;
    message?: string;
    request?: any;
}

export interface IPaginatedData<T> {
    nextPageToken: string;
    prevPageToken: string;
    pageInfo: {
        totalResults: number;
        resultsPerPage: number;
    };
    items: T[];
}

export interface IThumbnail {
    url: string;
    width: number;
    height: number;
}

export interface IId {
    kind: string;
    videoId: string;
    channelId: string;
    playlistId: string;
}

export interface ISnippet {
    publishedAt: string;
    channelId: string;
    title: string;
    description: string;
    thumbnails: Record<TThumbnailKey, IThumbnail>;
    channelTitle: string;
    liveBroadcastContent: TLiveBroadCastContent;
}

export interface IVideoSearchResult {
    id: IId;
    snippet: ISnippet;
}
