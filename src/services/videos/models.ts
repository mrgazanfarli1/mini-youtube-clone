import { IPaginatedData, IVideoSearchResult } from 'models';

export interface IVideoSearchResponse extends IPaginatedData<IVideoSearchResult> {
    kind: string;
    etag: string;
    regionCode: string;
}

export interface IVideoSearchParams {
    part?: string;
    type?: string;
    q?: string;
    pageToken?: string;
    maxResults?: number;
}
