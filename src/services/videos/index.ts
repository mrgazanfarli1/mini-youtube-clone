import { YOUTUBE_API_KEY } from 'consts';
import { IVideoSearchParams, IVideoSearchResponse } from 'services/videos/models';
import { getRequest } from 'utils/rest';

export const VideosServices = {
    getVideoSearchResults(params?: IVideoSearchParams): Promise<IVideoSearchResponse> {
        return getRequest('/search', { params: { ...params, key: YOUTUBE_API_KEY } });
    }
};
