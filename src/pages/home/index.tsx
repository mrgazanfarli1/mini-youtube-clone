import React from 'react';
import { VideoList } from 'components/video-list';

// although we do not have routing, I prefer to keep page wrappers separate
export const HomePage = (): JSX.Element => (
    <div>
        <VideoList />
    </div>
);
