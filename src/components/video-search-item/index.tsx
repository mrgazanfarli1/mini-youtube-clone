import React from 'react';
import { IVideoSearchResult } from 'models';
import moment from 'moment';
import './index.css';

interface IVideoSearchItemProps {
    onItemClick: (videoId: string) => void;
    item: IVideoSearchResult;
}

export const VideoSearchItem = ({ onItemClick, item: { id, snippet } }: IVideoSearchItemProps) => {
    const handleClick = React.useCallback(() => {
        onItemClick(id.videoId);
    }, [id.videoId, onItemClick]);

    return (
        <div className="video-search__root" key={id.videoId}>
            <div onClick={handleClick}>
                <div style={{ marginBottom: 16 }}>
                    <img width="100%" src={snippet.thumbnails.medium.url} alt={snippet.title} />
                </div>
                <div className="video-search__info" style={{ display: 'flex', marginBottom: 16 }}>
                    <div className="video-search__channel-logo" />
                    <div>
                        <div style={{ marginBottom: 8 }}>
                            <h6
                                style={{
                                    overflow: 'hidden',
                                    textOverflow: 'ellipsis',
                                    display: '-webkit-box',
                                    lineClamp: '2',
                                    WebkitLineClamp: '2',
                                    WebkitBoxOrient: 'vertical',
                                    boxOrient: 'vertical',
                                    fontSize: 16,
                                    lineHeight: '24px'
                                }}
                            >
                                {snippet.title}
                            </h6>
                        </div>
                        <p style={{ color: 'rgba(0, 0, 0, 0.6)', marginBottom: 4 }}>{snippet.channelTitle}</p>
                        <p style={{ color: 'rgba(0, 0, 0, 0.6)' }}>{moment(snippet.publishedAt).fromNow()}</p>
                    </div>
                </div>
                <div className="video-search__buttons">
                    <button className="video-search__button">Watch later</button>
                    <button className="video-search__button">Add to queue</button>
                </div>
            </div>
        </div>
    );
};
