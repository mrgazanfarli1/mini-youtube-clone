import React from 'react';
import InfiniteScroll from 'react-infinite-scroll-component';
import { useAsyncData } from 'hooks/useAsyncData';
import { ERequestStatus } from 'models/enums';
import { VideosServices } from 'services/videos';
import { IVideoSearchResponse } from 'services/videos/models';
import { VideoSearchItem } from 'components/video-search-item';

export const VideoList = (): JSX.Element => {
    const [videoSearchResult, setVideoSearchResult] = useAsyncData<IVideoSearchResponse>(true);

    const handleNextPageLoad = React.useCallback(() => {
        setVideoSearchResult(() =>
            VideosServices.getVideoSearchResults({
                part: 'snippet',
                type: 'video',
                q: 'programming',
                pageToken: videoSearchResult?.data?.nextPageToken,
                maxResults: 12
            })
        );
    }, [setVideoSearchResult, videoSearchResult?.data?.nextPageToken]);

    React.useEffect(() => {
        setVideoSearchResult(() =>
            VideosServices.getVideoSearchResults({
                part: 'snippet',
                type: 'video',
                q: 'programming',
                maxResults: 12
            })
        );
    }, [setVideoSearchResult]);

    const handleItemClick = React.useCallback((videoId: string) => {
        window.open(`https://youtube.com/watch?v=${videoId}`);
    }, []);

    let content;

    if (videoSearchResult.status === ERequestStatus.ERROR) {
        content = <h5 style={{ color: 'red' }}>Unexpected error occurred, please try again</h5>;
    } else {
        content = (
            <div style={{ padding: '48px 16px' }}>
                <InfiniteScroll
                    style={{ display: 'grid', gridTemplateColumns: 'repeat(4, 1fr)', gap: '0 20px' }}
                    next={handleNextPageLoad}
                    hasMore={!!videoSearchResult.data?.nextPageToken}
                    loader={null}
                    dataLength={videoSearchResult.data?.items?.length ?? 12}
                >
                    {videoSearchResult.data?.items?.map(item => (
                        <VideoSearchItem key={item.id.videoId} onItemClick={handleItemClick} item={item} />
                    ))}
                </InfiniteScroll>
            </div>
        );
    }

    return content;
};
