import * as React from 'react';
import { IAsyncData } from 'models';
import { ERequestStatus } from 'models/enums';

export function useAsyncData<T>(hasInfinitePages = false, initialData?: T) {
    const [state, setState] = React.useState<IAsyncData<T>>(() => {
        if (initialData) {
            return {
                status: ERequestStatus.SUCCESS,
                data: initialData,
                error: null
            };
        }

        return {
            status: ERequestStatus.IDLE,
            data: null,
            error: null
        };
    });

    const serviceCall = React.useCallback(
        (service: () => Promise<T>) => {
            setState(currentState => ({ ...currentState, status: ERequestStatus.PENDING, error: null }));

            return service()
                .then(response => {
                    setState(prevState => {
                        console.log(prevState, response);
                        return {
                            status: ERequestStatus.SUCCESS,
                            data: !hasInfinitePages
                                ? response
                                : {
                                      ...response,
                                      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                                      // @ts-ignore
                                      ...(response?.nextPageToken !== prevState?.data?.nextPageToken && {
                                          items: [
                                              // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                                              // @ts-ignore
                                              ...(prevState?.data?.items ?? []),
                                              // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                                              // @ts-ignore
                                              ...(response?.items ?? [])
                                          ]
                                      })
                                  },
                            error: null
                        };
                    });

                    return response;
                })
                .catch(error => {
                    setState({
                        status: ERequestStatus.ERROR,
                        data: error.data,
                        error: error.data
                    });

                    return Promise.reject(error);
                });
        },
        [hasInfinitePages]
    );

    const setToInitial = React.useCallback(() => {
        setState({
            status: ERequestStatus.IDLE,
            data: null,
            error: null
        });
    }, []);

    return [state, serviceCall, setToInitial] as const;
}
