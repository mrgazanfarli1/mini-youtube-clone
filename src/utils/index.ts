// Promise type guard
import { IError } from 'models';
import { Maybe } from 'models/types';

export function isPromise<T = any>(value: any): value is PromiseLike<T> {
    return value && typeof value.then === 'function';
}

export function isError(obj: any): obj is IError {
    return Boolean(obj) && (obj as IError).error === true;
}

export const isServerError = (error: Maybe<IError>) => `${error?.httpCode}`.startsWith('5');

export const getURLParam = (param: string, location: Location): any =>
    new URLSearchParams(location.search).get(param);

export const hasEnumValue = (enumToCheck, value: any) => (Object as any).values(enumToCheck).includes(value);
